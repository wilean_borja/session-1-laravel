<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Bug;

class BugController extends Controller
{
    public function index(){

    	$bugs = Bug::all();

    	return view('bugs', compact('bugs'));
    }
}
