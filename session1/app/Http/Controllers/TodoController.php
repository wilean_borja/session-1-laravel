<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Todo;
use \App\Category;

class TodoController extends Controller
{
    public function index(){

    	$tasks = Todo::all();
    	// dd($tasks);

    	return view('tasks', compact('tasks'));
    }

    public function create(){

    	$categories = Category::all();
    	// dd($tasks);

    	return view('add-task', compact('categories'));
    }

    public function store(Request $req){
    	$new_task = new Todo;
    	$new_task->title = $req->title;
    	$new_task->body = $req->body;
    	$new_task->category_id = $req->category_id;
    	$new_task->status_id = 1;
    	$new_task->user_id = 1;

    	$new_task->save();

    	return redirect('/tasks');
    }

    public function destroy($id){
    	// find data to delete then delete
    	$taskToDelete = Todo::find($id);
    	// dd($taskToDelete);
    	$taskToDelete->delete();
    	return redirect ('/tasks');
    }

    public function markAsDone($id){
    	// find the task to update then udpate
    	$taskToUpdate = Todo::find($id);

    	if($taskToUpdate->status_id == 3){
    		$taskToUpdate->status_id =1;
    	}else{
    		$taskToUpdate->status_id = 3;
    	}
    	
    	$taskToUpdate->save;

    	return redirect ('/tasks');
    }

}
